/*Теоретичні запитання*/

/*      1.Як можна оголосити змінну у Javascript?
      
        За допомогою ключових слів let, const (після присвоювання не змінюється)
        та var (застарілий варіант, використовувася до стандарту ES2015).
      
        2.У чому різниця між функцією prompt та функцією confirm?
      
        Встроєна функція promt надає користувачу ввести данні у полі (input) та
        підтвердити їх (Ok/true) або відмінити (Cancel/false). Функція confirm
        надає тільки можливість обрати наданий варіант.
      
        3.Що таке неявне перетворення типів? Наведіть один приклад.
      
        Оскільки у JS слабка типізація, у деяких випадках різні за типом
        значення конвертуються автоматично в один.
      
        Наприклад: +"3" + 3 = 6; (String стає Number без використання належної
        функції).
*/

document.querySelector(".questions");

let admin;

let name = "Max";

admin = name;
console.log(admin);

let days = 7;
console.log(Number(days * 24 * 60 * 60));

userRequest = prompt("Do you like JS", "Yes!");
console.log(userRequest);
